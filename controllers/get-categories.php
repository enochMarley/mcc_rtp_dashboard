<?php

    include_once "db-config.php";

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        $response = array();
        $categoriesArray = array();
        $allCategoriesResponse = array();

        //query to get the categories
        $getCategoriesQuery = "SELECT * FROM `rtp_categories` ORDER BY category_code ASC";

        $getCategoriesResult = mysqli_query($database, $getCategoriesQuery);

        if (mysqli_num_rows($getCategoriesResult) > 0) {
            while ($row = mysqli_fetch_assoc($getCategoriesResult)) {
               $categoriesArray['category_id'] = $row['category_id'];
               $categoriesArray['category_title'] = $row['category_title'];
               $categoriesArray['category_code'] = $row['category_code'];

               array_push($allCategoriesResponse, $categoriesArray);
            }

            $response['success'] = true;
        	$response["message"] = 'categories got';
        	$response["data"] = $allCategoriesResponse;

            header('Content-Type: application/json');
		    echo json_encode($response);
        } else {
            
        	$response['success'] = false;
            $response["message"] = 'No categories';
            $response['data'] = mysqli_num_rows($getCategoriesResult);

            header('Content-Type: application/json');
		    echo json_encode($response);
        }
    }