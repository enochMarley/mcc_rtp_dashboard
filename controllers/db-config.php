<?php
    $serverName = "localhost";
    $databaseName = "rtp_database";
    $databaseUser = "root";
    $databasePassword = "";

    $database = mysqli_connect($serverName, $databaseUser, $databasePassword, $databaseName);

    if (!$database) {
        die("unable to connect to database");
    }