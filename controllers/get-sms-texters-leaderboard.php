<?php

    include_once "db-config.php";

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        $response = array();
        $numberArray = array();
        $votesArray = array();
        $statArray = array();

        //query to get the categories
        $query = "SELECT DISTINCT `sender`, COUNT(`sender`) as sms_pay FROM `sms_pay` GROUP BY `sender` ORDER BY sms_pay DESC LIMIT 3 ";

        $result = mysqli_query($database, $query);

        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {

               array_push($numberArray, $row['sender']);
               array_push($votesArray, $row['sms_pay']);
            }

            $statArray['labels'] = $numberArray;
            $statArray['data'] = $votesArray;

            $response['success'] = true;
        	$response["message"] = 'stats got';
        	$response["data"] = $statArray;

            header('Content-Type: application/json');
		    echo json_encode($response);
        } else {
            
        	$response['success'] = false;
            $response["message"] = 'No categories';

            header('Content-Type: application/json');
		    echo json_encode($response);
        }
    }