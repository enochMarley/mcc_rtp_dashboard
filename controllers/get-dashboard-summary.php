<?php

    include_once "db-config.php";

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        $response = array();
        $statsArray = array();

        //query to get the  number categories
        $getNUmberOfCategoriesQuery = "SELECT category_code FROM rtp_categories";
        $getNUmberOfCategoriesResult = mysqli_query($database, $getNUmberOfCategoriesQuery);
        $numberOfCategories  = mysqli_num_rows($getNUmberOfCategoriesResult) | 0;

        // query to get the total number of nominees
        $getNumberOfNomineesQuery = "SELECT nominee_id FROM rtp_nominees";
        $getNUmberOfNomineesResult = mysqli_query($database, $getNumberOfNomineesQuery);
        $getNUmberOfNominees  = mysqli_num_rows($getNUmberOfNomineesResult) | 0;

        // query to get the total number of votes
        $getNumberOfRTPPayVotesQuery = "SELECT SUM(number_of_votes) AS rtp_vote_num FROM rtp_pay WHERE response_code = '0000'";
        $getNumberOfRTPPayVotesResult = mysqli_query($database, $getNumberOfRTPPayVotesQuery);
        $row1 = mysqli_fetch_assoc($getNumberOfRTPPayVotesResult);
        $getNumberOfRTPPay  = $row1['rtp_vote_num'] | 0;

        // query to get the total number of votes
        $getNumberOfSMSPayVotesQuery = "SELECT COUNT(sms_pay_id) sms_vote_num FROM sms_pay";
        $getNumberOfSMSPayVotesResult = mysqli_query($database, $getNumberOfSMSPayVotesQuery);
        $row2 = mysqli_fetch_assoc($getNumberOfSMSPayVotesResult);
        $getNumberOfSMSPay  = $row2['sms_vote_num']| 0;

        $statsArray["numberOfCategories"] = $numberOfCategories;
        $statsArray["getNUmberOfNominees"] = $getNUmberOfNominees;
        $statsArray["numberOfSMSVotes"] = $getNumberOfSMSPay;
        $statsArray["numberOfRTPVotes"] = $getNumberOfRTPPay;

        $response['success'] = true;
        $response["message"] = 'categories got';
        $response["data"] = $statsArray;

        header('Content-Type: application/json');
        echo json_encode($response);
    }