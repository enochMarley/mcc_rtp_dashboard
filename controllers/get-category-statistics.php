<?php

    include_once "db-config.php";

    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $categoryCode = trim($_POST["categoryCode"]);

        $response = array();
        $categoriesArray = array();
        $allCategoriesResponse = array();

        $nomineeNameArray = array();
        $nomineeVoteArray = array();
        $nomineeGraphRes = array();

        //query to get the category title
        $getCategoryTitleQuery = "SELECT category_title FROM  rtp_categories WHERE category_code = '$categoryCode'";

        $getCategoTitleResult = mysqli_query($database, $getCategoryTitleQuery);
        $getCategoTitleRow = mysqli_fetch_assoc($getCategoTitleResult);

        $categoryTitle = $getCategoTitleRow['category_title'];

        // query to get the statistics of the category title
        $getStatisticsQuery = "SELECT nominee_name, nominee_votes, nominee_thumbnail, nominee_region FROM rtp_nominees WHERE nominee_category LIKE '%$categoryTitle%' ORDER BY nominee_votes DESC";

        $getStatisticsResult =  mysqli_query($database, $getStatisticsQuery);

        if (mysqli_num_rows($getStatisticsResult) > 0) {
            
            while ($row = mysqli_fetch_assoc($getStatisticsResult)) {
               $categoriesArray['nominee_name'] = $row['nominee_name'];
               $categoriesArray['nominee_votes'] = $row['nominee_votes'];
               $categoriesArray['nominee_thumbnail'] = $row['nominee_thumbnail'];
               $categoriesArray['nominee_region'] = $row['nominee_region'];

               array_push($allCategoriesResponse, $categoriesArray);

               array_push($nomineeNameArray, $row['nominee_name']);
               array_push($nomineeVoteArray, $row['nominee_votes']);
            }

            $nomineeGraphRes['labels'] = $nomineeNameArray;
            $nomineeGraphRes['data'] = $nomineeVoteArray;

            $response['success'] = true;
        	$response["message"] = 'categories got';
            $response["data"] = $allCategoriesResponse;
            $response['graph'] = $nomineeGraphRes;
            $response['categoryTitle']= $categoryTitle;

            header('Content-Type: application/json');
		    echo json_encode($response);
        } else {
            
        	$response['success'] = false;
            $response["message"] = 'No categories';
            

            header('Content-Type: application/json');
		    echo json_encode($response);
        }
    }