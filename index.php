<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>RTP Awards</title>
        <link href="assets/css/materialize.min.css" rel="stylesheet">
        <link href="assets/css/toastr.min.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="main-div">
            <div class="row">
                <div class="col l4"></div>
                <div class="col l4">
                    <img src="assets/img/logo.png" class="auth-page-logo">
                    <h4 class="center-align text-custom">Login</h4>
                    <form class="col s12 login-form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="input-field col s12">
                              <input id="first_name" type="text" class="validate login-username">
                              <label for="first_name">username</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                              <input id="password" type="password" class="validate login-password">
                              <label for="password">Password</label>
                            </div>
                        </div>
                        <button class="btn waves-effect waves-light bg-custom" type="submit" name="action">Login</button>
                        <br><br><br>
                        
                    </form>
                </div>
                <div class="col l4"></div>
            </div>
        </div>
    </body>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/materialize.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>
    <script src="assets/js/index.js"></script>
    <script src="assets/js/auth-controller.js"></script>
</html>