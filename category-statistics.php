<?php
    session_start();
    if (!isset($_SESSION['userLoggedIn'])) {
        echo "<script>window.location.href = 'index.php';</script>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>RTP Awards</title>
        <link href="assets/css/materialize.min.css" rel="stylesheet">
        <link href="assets/css/toastr.min.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar-fixed">
            <nav class="navbar-custom">
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo"><img src="assets/img/logo.png" class="navbar-img"></a>
                    <a href="#" data-target="mobile-demo" class="sidenav-trigger">&#9776;</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><img src="assets/img/mcc-logo.png" alt="" class="navbar-img"></li>
                    </ul>
                </div>
            </nav>
        </div>

        <ul class="sidenav custom-side-nav" id="mobile-demo">
            <li><a href="dashboard.php"> Dashboard</a></li>
            <li class="selected"><a href="nominees.php"> Categories &amp; Nominations</a></li>
            <li><a href="logout.php"> Logout</a></li>
        </ul>

        <div>
            <div class="row">
                <div class="col s2 side-div">
                    <li><a href="dashboard.php"> Dashboard</a></li>
                    <li class="selected"><a href="nominees.php"> Categories &amp; Nominations</a></li>
                    <br>
                    <li><a href="logout.php"> Logout</a></li>
                </div>

                <div class="col s10 content-div wow slideInDown"  data-wow-duration='0.5s' data-wow-delay='0.3s'>
                    <br>
                    <h6 class="center-align text-custom">Statistics For <span class="cateogory-title"></span></h6>

                    <div class="row">
                        <div class="col m4 l4">
                            <div class="nominees-table-res"></div>
                        </div>
                        <div class="col m8 l8 s12">
                            <div class="nominees-graph-res">
                                <canvas id="nomineeChart" width="400" height="180"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/materialize.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>
    <script src="assets/js/Chart.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/index.js"></script>
    <script src="assets/js/controller.js"></script>
    <script>
        new WOW().init();
        getCategoryStatistics();
    </script>
</html>