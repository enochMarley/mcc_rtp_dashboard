-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2018 at 01:54 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtp_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `rtp_dashboard_users`
--

CREATE TABLE `rtp_dashboard_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_signup_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rtp_dashboard_users`
--

INSERT INTO `rtp_dashboard_users` (`user_id`, `username`, `user_password`, `user_signup_date`) VALUES
(2, 'username', '$2y$10$s/vpOXJuonDfeHTFY/zqKOXHxOVNlyUNUdH1DoEfFnIyTqtJbo5nG', '2018-08-10 22:30:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rtp_dashboard_users`
--
ALTER TABLE `rtp_dashboard_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rtp_dashboard_users`
--
ALTER TABLE `rtp_dashboard_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
