<?php
    session_start();
    if (!isset($_SESSION['userLoggedIn'])) {
        echo "<script>window.location.href = 'index.php';</script>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>RTP Awards</title>
        <link href="assets/css/materialize.min.css" rel="stylesheet">
        <link href="assets/css/toastr.min.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar-fixed">
            <nav class="navbar-custom">
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo"><img src="assets/img/logo.png" class="navbar-img"></a>
                    <a href="#" data-target="mobile-demo" class="sidenav-trigger">&#9776;</a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><img src="assets/img/mcc-logo.png" alt="" class="navbar-img"></li>
                    </ul>
                </div>
            </nav>
        </div>

        <ul class="sidenav custom-side-nav" id="mobile-demo">
            <li class="selected"><a href="#"> Dashboard</a></li>
            <li><a href="nominees.php"> Categories &amp; Nominations</a></li>
            <li><a href="logout.php"> Logout</a></li>
        </ul>

        <div>
            <div class="row">
                <div class="col s2 side-div">
                    <li class="selected"><a href="#"> Dashboard</a></li>
                    <li><a href="nominees.php"> Categories &amp; Nominations</a></li>
                    <br>
                    <li><a href="logout.php"> Logout</a></li>
                </div>

                <div class="col s10 content-div">
                    <div class="row">
                        <div class="col s12 m3 l3 wow zoomIn" data-wow-duration='0.5s' data-wow-delay='0.2s'>
                            <div class="dashboard-summary-item orange lighten-4">
                               <h4 class="number-of-categories"><img src="assets/img/loader.svg"></h4>
                               <p>Categories</p>
                            </div>
                        </div>

                        <div class="col s12 m3 l3 wow zoomIn" data-wow-duration='0.5s' data-wow-delay='0.3s'>
                            <div class="dashboard-summary-item  red lighten-4">
                               <h4 class="number-of-nominees"><img src="assets/img/loader.svg"></h4>
                               <p>Nominees</p>
                            </div>
                        </div> 

                        <div class="col s12 m3 l3 wow zoomIn" data-wow-duration='0.5s' data-wow-delay='0.5s'>
                            <div class="dashboard-summary-item dashboard-summary-duo-item  purple lighten-4">
                                <div>
                                    <h4 class="number-of-sms-votes"><img src="assets/img/loader.svg"></h4>
                                    <p><small>SMS Vote Counts</small></p>
                                </div>

                                <div>
                                    <h4 class="number-of-mom-votes"><img src="assets/img/loader.svg"></h4>
                                    <p><small>MoMo Vote Counts</small></p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 l3 wow zoomIn" data-wow-duration='0.5s' data-wow-delay='0.4s'>
                            <div class="dashboard-summary-item  blue-grey lighten-4">
                               <h4 class="number-of-votes"><img src="assets/img/loader.svg"></h4>
                               <p>Total Vote Counts</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="center-align text-custom">Highest Voters</p>

                        <div class="col s12 m6 l6">
                            <p class="center-align white-text">Mobile Money</p>
                            <canvas id="rptpaygraph" width="300" height="180"></canvas>
                        </div>

                        <div class="col s12m6 l6 leaderboard-graph-div">
                            <p class="center-align white-text">SMS</p>
                            <canvas id="smspaygraph" width="300" height="180"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/materialize.min.js"></script>
    <script src="assets/js/toastr.min.js"></script>
    <script src="assets/js/Chart.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/index.js"></script>
    <script src="assets/js/controller.js"></script>
    <script>
        new WOW().init();
        getDashboardSummary();
        getMobileMoneyTextersLeaderBoard();
        getSMSTextersLeaderBoard();
    </script>
</html>