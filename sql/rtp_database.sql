-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2018 at 12:58 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtp_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `rtp_categories`
--

CREATE TABLE `rtp_categories` (
  `category_id` int(11) NOT NULL,
  `category_title` text NOT NULL,
  `category_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rtp_categories`
--

INSERT INTO `rtp_categories` (`category_id`, `category_title`, `category_code`) VALUES
(1, 'TV STATION OF THE YEAR', 'A1'),
(2, 'Media Group Of The Year', 'A2'),
(3, 'Digital TV Channel Of The Year', 'A3'),
(4, 'RTP Personality Of The Year', 'A4'),
(5, 'Radio Morning Show Host Of The Year', 'A5'),
(6, 'Radio Late Afternoon Show Host Of The Year', 'A6'),
(7, 'Radio Mid-Morning Show Host Of The Year', 'A7'),
(8, 'Radio DJ Of The Year ', 'A8'),
(9, 'Sports Show Host Of The Year ', 'A9'),
(10, 'Radio Reggae Show Host Of The Year ', 'B1'),
(11, 'Radio Gospel Show Host Of The Year', 'B2'),
(12, 'Radio Entertainment Talk Show Host ', 'B3'),
(13, 'Radio Newscaster of the Year (Local Language) ', 'B4'),
(14, 'Radio Newscaster of the Year (English Language)', 'B5'),
(15, 'Radio Devt Show Host Of The Year 2017-2018', 'B6'),
(16, 'Radio Program Of The Year 2017-2018', 'B7'),
(17, 'Radio Sports Program Of The Year 2017-2018', 'B8'),
(18, 'Radio News Program Of The Year 2017-2018', 'B9'),
(19, 'Best Radio Personality Brong Ahafo Region 2017-2018', 'C9'),
(20, 'Best Radio Personality Northern Sector Of Ghana 2017-2018', 'C1'),
(21, 'Best Radio Personality Central Region 2017-2018', 'C2'),
(22, 'Best Radio Personality Western Region 2017-2018', 'C3'),
(23, 'Best Radio Personality Eastern Region 2017-2018', 'C4'),
(24, 'Best Radio Personality Volta Region 2017-2018', 'C5'),
(25, 'Best Radio Personality Greater Accra Region 2017-2018', 'C6'),
(26, 'Best Radio Personality Ashanti Region 2017-2018', 'C7'),
(27, 'Radio Morning Show Of The Year 2017-2018', 'C8'),
(28, 'TV Male Newscaster Of The Year 2017-2018', 'D1'),
(29, 'TV Female Newscaster Of The Year 2017-2018', 'D2'),
(30, 'TV Morning Show Host Of The Year 2017-2018', 'D3'),
(31, 'TV Sports Show Host Of The Year 2017-2018', 'D4'),
(33, 'TV Devt Show Host Of The Year 2017-2018', 'D6'),
(34, 'TV Male Entertainment Show Host Of The Year 2017-2018', 'D7'),
(35, 'TV Female Entertainment Show Host Of The Year 2017-2018', 'D8'),
(36, 'TV Program Of The Year 2017-2018', 'D9'),
(37, 'Television Morning Show Of The Year 2017-2018', 'E1'),
(38, 'TV Entertainment Show Of The Year 2017-2018', 'E2'),
(39, 'TV Sports Program Of The Year 2017-2018', 'E3'),
(40, 'TV News Program Of The Year 2017-2018', 'E4');

-- --------------------------------------------------------

--
-- Table structure for table `rtp_dashboard_users`
--

CREATE TABLE `rtp_dashboard_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_signup_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rtp_dashboard_users`
--

INSERT INTO `rtp_dashboard_users` (`user_id`, `username`, `user_password`, `user_signup_date`) VALUES
(2, 'username', '$2y$10$s/vpOXJuonDfeHTFY/zqKOXHxOVNlyUNUdH1DoEfFnIyTqtJbo5nG', '2018-08-10 22:30:36'),
(3, 'username1', '$2y$10$8vXcXWFQiZRYDWsD5PHx.u3yIXKvZ0WhsZnWdxiFFlsVDwDAKnysu', '2018-08-13 12:58:05'),
(4, 'user', '5f4dcc3b5aa765d61d8327deb882cf99', '2018-08-17 14:52:17');

-- --------------------------------------------------------

--
-- Table structure for table `rtp_nominees`
--

CREATE TABLE `rtp_nominees` (
  `nominee_id` int(11) NOT NULL,
  `nominee_name` text NOT NULL,
  `nominee_votes` int(11) NOT NULL,
  `nominee_thumbnail` text NOT NULL,
  `nominee_video1` text NOT NULL,
  `nominee_bio` text NOT NULL,
  `nominee_number` varchar(11) NOT NULL,
  `nominee_region` text NOT NULL,
  `nominee_category` text NOT NULL,
  `season` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rtp_nominees`
--

INSERT INTO `rtp_nominees` (`nominee_id`, `nominee_name`, `nominee_votes`, `nominee_thumbnail`, `nominee_video1`, `nominee_bio`, `nominee_number`, `nominee_region`, `nominee_category`, `season`) VALUES
(1, 'ADOM TV ', 16, 'http://mysmsinbox.com/rtp_api/nominees/adom_tv.jpg', '', 'None', '', 'Greater Accra', 'TV STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(2, ' UTV', 14, 'http://mysmsinbox.com/rtp_api/nominees/utv.png', '', 'None', '', 'Greater Accra', 'TV STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(3, 'KWESE TV', 7, 'http://mysmsinbox.com/rtp_api/nominees/kwese_freesports_l.png', '', 'None', '', 'Greater Accra', 'TV STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(4, ' TV3', 4, 'http://mysmsinbox.com/rtp_api/nominees/tv3.png', '', 'None', '', 'Greater Accra', 'TV STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(5, 'ANGEL TV ', 50, 'http://mysmsinbox.com/rtp_api/nominees/angel_tv.png', '', 'None', '', 'Greater Accra', 'TV STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(6, 'GHONE TV      ', 1, 'http://mysmsinbox.com/rtp_api/nominees/ghone.png', '', 'None', '', 'Greater Accra', 'TV STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(7, 'AMANSAN TV ', 51, 'http://mysmsinbox.com/rtp_api/nominees/atv.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(8, 'TV XYZ', 1, 'http://mysmsinbox.com/rtp_api/nominees/tv_xyz.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(9, 'PRAISE TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/priase_tv_logo.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(10, 'OBTV ', 0, 'http://mysmsinbox.com/rtp_api/nominees/obtv.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(11, 'ANGEL TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/angel_png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(12, 'ADOM TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/adom_tv.jpg', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(13, '4SYTE TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/4syte.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(14, 'PAN AFRICAN TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(15, 'AGOO TV', 1, 'http://mysmsinbox.com/rtp_api/nominees/agoo_tv_logo.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(16, 'ICE TV ', 0, 'http://mysmsinbox.com/rtp_api/nominees/icetv.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(17, 'ATINKA TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/atinka_tv_logo.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(18, 'MAX TV', 0, 'http://mysmsinbox.com/rtp_api/nominees/max_tv.png', '', 'None', '', 'Greater Accra', 'Digital TV Channel Of The Year', '0000-00-00 00:00:00'),
(19, 'STATE OF AFFAIRS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/state_of_affairs_ghone.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(20, 'GH TODAY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/ghtoday.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(21, 'HOME RUN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KWESE TV ', '', 'Greater Accra', 'TV Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(22, ' NEWS 360 ', 0, 'http://mysmsinbox.com/rtp_api/nominees/news_360_tv3.png', '', 'TV3', '', 'Greater Accra', 'TV Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(23, 'ADEKYE NSROMA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/adekye_nsroma.png', '', 'UTV', '', 'Greater Accra', 'TV Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(24, 'RESTORATION WITH STACY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/restoration_with_stacy.jpg', '', 'PLATINUM NETWORK', '', 'Greater Accra', 'TV Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(25, 'YAA KONAMA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/yaa_ankoma_utv.png', '', 'UTV', '', 'Greater Accra', 'TV Morning Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(26, 'JOHNNIE HUGHES ', 0, 'http://mysmsinbox.com/rtp_api/nominees/johnnie_hughes_tv3.png', '', 'TV3', '', 'Greater Accra', 'TV Morning Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(27, 'OMANHENE KWABENA ASANTE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/omanhene_kwabena_asante.png', '', 'ADOM TV ', '', 'Greater Accra', 'TV Morning Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(28, 'BAISIWAA DOWUNA HAMMOND ', 0, 'http://mysmsinbox.com/rtp_api/nominees/baisiwa_dowuona_hammond.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Morning Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(29, 'BISMARK BROWN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bismark_brown_atinka_tv.png', '', 'ATINKA TV', '', 'Greater Accra', 'TV Morning Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(30, 'TWENEBOA KODUAH (TK) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/tweneboah_koduah_utv.png', '', 'UTV', '', 'Greater Accra', 'TV Morning Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(31, 'BADWAM', 1, 'http://mysmsinbox.com/rtp_api/nominees/badwam_adom_tv.jpg', '', 'ADOM TV', '', 'Greater Accra', 'Television Morning Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(32, 'GH TODAY ', 1, 'http://mysmsinbox.com/rtp_api/nominees/ghtoday.png', '', 'GHONE', '', 'Greater Accra', 'Television Morning Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(33, 'NEW DAY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/new_day_tv3.png', '', 'TV3', '', 'Greater Accra', 'Television Morning Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(34, 'BREAKFAST LIVE', 0, 'http://mysmsinbox.com/rtp_api/nominees/breakfast_live_tv_africa.png', '', 'TV AFRICA', '', 'Greater Accra', 'Television Morning Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(35, 'ADEHYE NSROMA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/adekye_nsroma.png', '', 'UTV', '', 'Greater Accra', 'Television Morning Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(36, 'GOOD MORNING GHANA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/good_morning_ghana.png', '', 'METRO TV', '', 'Greater Accra', 'Television Morning Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(37, 'YAW AMPOFO ANKRAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/yaw_amofo.png', '', 'KWESE TV', '', 'Greater Accra', 'TV Sports Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(38, 'THIERRY NYANN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/thierry_nyann.png', '', 'TV3', '', 'Greater Accra', 'TV Sports Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(39, 'PATRICK OSEI AGYEMANG (SONGO) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/songo_1.png', '', 'ADOM TV ', '', 'Greater Accra', 'TV Sports Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(40, 'KOKUI SELORMEY HANSON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kokui.png', '', 'KWESE TV', '', 'Greater Accra', 'TV Sports Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(41, 'SADDICK ADAMS (SPORTS OBAMA) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/saddick_adams.png', '', 'ATINKA TV ', '', 'Greater Accra', 'TV Sports Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(42, 'MICHAEL ODURO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/michael_oduro.png', '', 'METRO TV', '', 'Greater Accra', 'TV Sports Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(43, 'HOME RUN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/home_run.png', '', 'KWESE TV', '', 'Greater Accra', 'TV Sports Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(44, 'ANGEL SPORTS LIVE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ANGEL TV', '', 'Greater Accra', 'TV Sports Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(45, 'FIRE FOR FIRE', 0, 'http://mysmsinbox.com/rtp_api/nominees/fire_for_fire.png', '', 'ADOM TV', '', 'Greater Accra', 'TV Sports Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(46, 'HEAD START ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KWESE TV ', '', 'Greater Accra', 'TV Sports Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(47, 'CHEERS', 0, 'http://mysmsinbox.com/rtp_api/nominees/cheers.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Sports Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(48, 'JOY SPORTS TODAY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/joy_sports.jfif', '', 'JOY NEWS', '', 'Greater Accra', 'TV Sports Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(49, 'AKUA AMOAKOWAA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/akua_amoakowaa.png', '', 'ANGEL TV', '', 'Greater Accra', 'TV Female Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(50, 'REGINA VAN-HELVERT ', 0, 'http://mysmsinbox.com/rtp_api/nominees/regina_helvert.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Female Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(51, 'GLORIA SARFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/gloria_sarfo.png', '', 'ANGEL TV', '', 'Greater Accra', 'TV Female Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(52, 'AFIA SCHWARZNEGER ', 0, 'http://mysmsinbox.com/rtp_api/nominees/afia_schwarzneger.png', '', 'TV AFRICA', '', 'Greater Accra', 'TV Female Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(53, 'ANITA AKUFFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/anita_akuffo.png', '', 'GHONE TV ', '', 'Greater Accra', 'TV Female Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(54, 'BELLA MUNDI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bella_mundi.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Female Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(55, 'OFFICIAL KWAME ', 0, 'http://mysmsinbox.com/rtp_api/nominees/official_kwame.png', '', 'TV3 (3MUSIC NETWORK)', '', 'Greater Accra', 'TV Male Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(56, 'ABEIKU SANTANA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/santana.png', '', 'UTV', '', 'Greater Accra', 'TV Male Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(57, 'BROWN BERRY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'TV7 (ZEEX MUSIC TV)', '', 'Greater Accra', 'TV Male Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(58, 'JASON ELA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/jason_ela.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Male Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(59, 'KPEKPO MAXWELL JUSTICE (KMJ) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY PRIME', '', 'Greater Accra', 'TV Male Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(60, 'GIOVANNI CALEB ', 0, 'http://mysmsinbox.com/rtp_api/nominees/giovanni_calleb.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Male Entertainment Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(61, 'PLAYLIST ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'METRO TV', '', 'Greater Accra', 'TV Entertainment Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(62, 'THE PURPLE ROOM', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ANGEL TV ', '', 'Greater Accra', 'TV Entertainment Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(63, 'RYTHMZ LIVE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rythmz_live.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Entertainment Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(64, '3MUSIC FLAVA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/3_music.jfif', '', 'TV3 (3MUSIC NETWORK', '', 'Greater Accra', 'TV Entertainment Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(65, 'ZEEX TV SHOW ', 0, 'http://mysmsinbox.com/rtp_api/nominees/zeex_tv_show.png', '', 'TV7 (ZEEX TV PRODUCTION)', '', 'Greater Accra', 'TV Entertainment Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(66, 'THE LATE AFTERNOON SHOW ', 0, 'http://mysmsinbox.com/rtp_api/nominees/berlamundi.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Entertainment Show Of The Year 2017-2018', '0000-00-00 00:00:00'),
(67, 'NANA AMA MCBROWN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_mcbrown.png', '', 'UTV', '', 'Greater Accra', 'TV Devt Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(68, 'AKUMAA MAMA ZIMBI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/akumaa_mama_zimbi.png', '', 'ADOM TV', '', 'Greater Accra', 'TV Devt Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(69, 'BELLA MUNDI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bella_mundi.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Devt Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(70, 'STACY AMOATENG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/stacy_amoateng.jpg', '', 'PLATINUM NETWORK', '', 'Greater Accra', 'TV Devt Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(71, 'NANA ABA ANAMOAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_aba_anamoa.jpg', '', 'GHONE TV ', '', 'Greater Accra', 'TV Devt Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(72, 'SELLY GALLEY FIAWOO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/selly_fiawoo.png', '', 'JOY PRIME', '', 'Greater Accra', 'TV Devt Show Host Of The Year 2017-2018', '0000-00-00 00:00:00'),
(73, 'NATALIE FORT ', 0, 'http://mysmsinbox.com/rtp_api/nominees/natalie_fort.png', '', ' TV3', '', 'Greater Accra', 'TV Female Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(74, 'AMA SARPONG KUMANKUMA', 0, 'http://mysmsinbox.com/rtp_api/nominees/ama_sarpong_kumankuma.png', '', 'UTV', '', 'Greater Accra', 'TV Female Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(75, 'GIFTY ANDOH APPIAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/gifty_andoh.png', '', 'JOY NEWS', '', 'Greater Accra', 'TV Female Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(76, 'NANA YAA BREFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_yaa_brefo.png', '', 'ADOM TV', '', 'Greater Accra', 'TV Female Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(77, 'SERWAA AMIHERE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/serwaa_amihere11.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Female Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(78, 'KOBINA AMOONO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kobina_amoono.jpg', '', 'ANGEL TV', '', 'Greater Accra', 'TV Male Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(79, 'AGYA KWABENA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/agya_kwabena.jfif', '', 'UTV', '', 'Greater Accra', 'TV Male Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(80, 'PATRICK KWABENA STEPHENSON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/patrick_kwabena_stephenson.png', '', 'GHONE TV', '', 'Greater Accra', 'TV Male Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(81, 'ALFRED OCANSEY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/alfred_ocansey.png', '', 'TV3', '', 'Greater Accra', 'TV Male Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(82, 'KWAKU ADU KUMI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kweku_adu_kumi.jpg', '', 'ADOM TV', '', 'Greater Accra', 'TV Male Newscaster Of The Year 2017-2018', '0000-00-00 00:00:00'),
(83, 'KOKUI SELORMEY HANSON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kokui.png', '', 'KWESE TV', '', 'Greater Accra', 'TV FEMALE PRESENTER OF THE YEAR ', '0000-00-00 00:00:00'),
(84, 'STACY AMOATENG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/stacy_amoateng.jpg', '', 'PLATINUM NETWORK', '', 'Greater Accra', 'TV FEMALE PRESENTER OF THE YEAR ', '0000-00-00 00:00:00'),
(85, 'NANA ABA ANAMOAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_aba_anamoa.jpg', '', 'GHONE TV', '', 'Greater Accra', 'TV FEMALE PRESENTER OF THE YEAR ', '0000-00-00 00:00:00'),
(86, 'GIFTY ANDOH APPIAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/gifty_andoh.png', '', 'JOY NEWS ', '', 'Greater Accra', 'TV FEMALE PRESENTER OF THE YEAR ', '0000-00-00 00:00:00'),
(87, 'BERLA MUNDI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/berla_mundi.png', '', 'GHONE TV', '', 'Greater Accra', 'TV FEMALE PRESENTER OF THE YEAR ', '0000-00-00 00:00:00'),
(88, 'NANA AMA MCBROWN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_ama_mcbrown.png', '', ' UTV ', '', 'Greater Accra', 'TV FEMALE PRESENTER OF THE YEAR ', '0000-00-00 00:00:00'),
(89, 'JOY NEWS (THE PULSE)', 0, 'http://mysmsinbox.com/rtp_api/nominees/joy_news.png', '', 'JOY NEWS', '', 'Greater Accra', 'TV News Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(90, 'NEWS 360', 0, 'http://mysmsinbox.com/rtp_api/nominees/news_360_tv3.png', '', 'TV3', '', 'Greater Accra', 'TV News Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(91, 'ADOM PREMTUBRE KASEA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/adom_kase3.png', '', 'ADOM TV', '', 'Greater Accra', 'TV News Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(92, 'UTV PRIME NEWS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/.png', '', ' UTV', '', 'Greater Accra', 'TV News Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(93, 'NEWS TONIGHT ', 0, 'http://mysmsinbox.com/rtp_api/nominees/ghone_news.png', '', 'GHONE TV', '', 'Greater Accra', 'TV News Program Of The Year 2017-2018', '0000-00-00 00:00:00'),
(94, 'NANA ABA ANAMOAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_aba_anamoa.jpg', '', 'GHONE TV', '', 'Greater Accra', 'RTP PERSONALITY OF THE YEAR', '0000-00-00 00:00:00'),
(95, 'STACY AMOATENG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/stacy_amoateng.jpg', '', 'PLATINUM NETWORK', '', 'Greater Accra', 'RTP PERSONALITY OF THE YEAR', '0000-00-00 00:00:00'),
(96, 'NANA YAW SARFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_yaw_sarfo.png', '', 'VISION 1 FM', '', 'Greater Accra', 'RTP PERSONALITY OF THE YEAR', '0000-00-00 00:00:00'),
(97, 'PATRICK OSEI AGYEMAN (SONGO)', 0, 'http://mysmsinbox.com/rtp_api/nominees/songo_1.png', '', 'ADOM TV', '', 'Greater Accra', 'RTP PERSONALITY OF THE YEAR', '0000-00-00 00:00:00'),
(98, 'BERNARD AVLE ', 1, 'http://mysmsinbox.com/rtp_api/nominees/bernard_avle.png', '', 'CITI FM', '', 'Greater Accra', 'RTP PERSONALITY OF THE YEAR', '0000-00-00 00:00:00'),
(99, 'MULTIMEDIA GROUP', 0, 'http://mysmsinbox.com/rtp_api/nominees/multimedia.png', '', 'None', '', 'Greater Accra', 'MEDIA GROUP OF THE YEAR', '0000-00-00 00:00:00'),
(100, 'DESPITE MEDIA GROUP', 0, 'http://mysmsinbox.com/rtp_api/nominees/despite.png', '', 'None', '', 'Greater Accra', 'MEDIA GROUP OF THE YEAR', '0000-00-00 00:00:00'),
(101, 'ANGEL MEDIA GROUP', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'None', '', 'Greater Accra', 'MEDIA GROUP OF THE YEAR', '0000-00-00 00:00:00'),
(102, 'EIB NETWORK', 1, 'http://mysmsinbox.com/rtp_api/nominees/eib_network.png', '', 'None', '', 'Greater Accra', 'MEDIA GROUP OF THE YEAR', '0000-00-00 00:00:00'),
(103, 'MEDIA GENERAL GROUP', 0, 'http://mysmsinbox.com/rtp_api/nominees/media_general.png', '', 'None', '', 'Greater Accra', 'MEDIA GROUP OF THE YEAR', '0000-00-00 00:00:00'),
(104, 'DR. OFORI SARPONG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'OWNER OF OKAY FM AND UTV', '', 'Greater Accra', 'RTP HONORARIES OF THE YEAR 2018', '0000-00-00 00:00:00'),
(105, 'MR. FRANCIS KWABENA POKU (AFRO) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'OWNER OF HOT FM (ACCRA) AND FOX FM (KUMASI)', '', 'Greater Accra', 'RTP HONORARIES OF THE YEAR 2018', '0000-00-00 00:00:00'),
(106, 'ADOM FM', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'None', '', 'Greater Accra', 'RADIO STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(107, 'PEACE FM', 0, 'http://mysmsinbox.com/rtp_api/nominees/peace.png', '', 'None', '', 'Greater Accra', 'RADIO STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(108, 'STARR FM', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'None', '', 'Greater Accra', 'RADIO STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(109, 'CITI FM', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'None', '', 'Greater Accra', 'RADIO STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(110, 'ANGEL FM ', 0, 'http://mysmsinbox.com/rtp_api/nominees/angel_fm.png', '', 'None', '', 'Asanti Region', 'RADIO STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(111, 'JOY FM', 0, 'http://mysmsinbox.com/rtp_api/nominees/joyfm.png', '', 'None', '', 'Greater Accra', 'RADIO STATION OF THE YEAR ', '0000-00-00 00:00:00'),
(112, 'SUPER MORNING SHOW ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'RADIO PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(113, 'STARR CHART ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(114, 'ANGEL SPORTS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ANGEL FM', '', 'Ashanti Region', 'RADIO PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(115, 'GHANA BEYE YIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'VISION 1 FM', '', 'Greater Accra', 'RADIO PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(116, 'STARR DRIVE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(117, 'FRANCIS ABBAN ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(118, 'AGYEMANG PREMPEH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'POWER FM', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(119, 'BERNARD AVLE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bernard_avle.png', '', 'CITI FM', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(120, 'AFIA POKUAA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/afiaeee.png', '', 'ADOM FM', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(121, 'NANA YAW SARFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'VISION 1 FM', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(122, 'KWAME NKRUMAH TIKESIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kwame_nkrumah_tikesie.png', '', 'OKAY FM', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(123, 'WINSTON AMOAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', ' 3FM ', '', 'Greater Accra', 'BEST RADIO PERSONALITY GREATER ACCRA REGION 2017-2018', '0000-00-00 00:00:00'),
(124, 'BRIGHT KANKAM BOADU ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bright_ellis.png', '', 'NHYIRA FM', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(125, 'OMANHENE BOAKYIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/omanhene_boakyie.png', '', 'KESSBEN FM', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(126, 'KWAME TANKO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ANGEL FM', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(127, 'IKE DE UNPREDICTABLE', 0, 'http://mysmsinbox.com/rtp_api/nominees/ike_de_inpredictable.png', '', 'ABUSUA FM', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(128, 'MICHAEL DARKO (SUMMER) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/michael_darko.png', '', 'ANGEL FM', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(129, 'EBENEZER NANA YAW DONKOR (NYDJ) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/ebenezer_nana_yaw_donkor.png', '', 'YFM (KUMASI)', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(130, 'JOE LAKAR ', 0, 'http://mysmsinbox.com/rtp_api/nominees/joe_3.png', '', 'KESSBEN FM', '', 'Ashanti Region', 'BEST RADIO PERSONALITY ASHANTI REGION 2017-2018', '0000-00-00 00:00:00'),
(131, ' KWAKU DAWURO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/dawuro.png', '', 'KINGDOM FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(132, 'LONDONA NIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/london.png', '', 'KINGDOM FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(133, 'EUGUNE AKOTO BAMFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'AGOO FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(134, 'DR PREKESE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/prekese.png', '', 'BRYT FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(135, 'JUSTINA', 0, 'http://mysmsinbox.com/rtp_api/nominees/justina.png', '', 'OBOUBA FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(136, 'PAPA ATTITUDE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/attitude.png', '', 'RITE FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(137, 'CELESTINE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'RITE FM', '', 'Eastern Region', 'BEST RADIO PERSONALITY EASTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(138, 'KWESI GYESI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'FILLA FM', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(139, 'YARO ISHMAIL ', 0, 'http://mysmsinbox.com/rtp_api/nominees/yaro.png', '', 'MIGHT FM', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(140, 'SEY MUBARRAK ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ANGEL FM', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(141, 'DAKURA ALASKA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/alaska.png', '', 'PUPELI FM ', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(142, 'ASPERILLA FOSTINO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/aspe.png', '', 'SUNGMAALE FM', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(143, 'ANAAKAA WARRIS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'RADIO PROGRESS', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(144, 'AZIZ IBRAHIM ', 0, 'http://mysmsinbox.com/rtp_api/nominees/aziz_ibrahim.png', '', 'A1 RADIO', '', 'Northen Sector', 'BEST RADIO PERSONALITY NORTHERN SECTOR OF GHANA 2017-2018', '0000-00-00 00:00:00'),
(145, 'ADU GYAMFI MARFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/adu_gyamfi_marfo', '', 'OSAGYEFO FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(146, 'KWAME DAPAA ASARE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kwame_dapaa_asare.png', '', 'OKOKROKO FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(147, 'NAK ABRANTSE-KWANTENPON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nac_abrantse.png', '', 'RICH FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(148, 'SIR WALKER', 0, 'http://mysmsinbox.com/rtp_api/nominees/sir_walker.png', '', 'NKWA FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(149, 'CONSCIOUS QUEEN', 0, 'http://mysmsinbox.com/rtp_api/nominees/queen.png', '', 'PINK FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(150, 'QUEEN MABEL ', 0, 'http://mysmsinbox.com/rtp_api/nominees/queen_mabel.png', '', 'CAPE FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(151, 'NANA SANKAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'EAGLE FM', '', 'Central Region', 'BEST RADIO PERSONALITY CENTRAL REGION 2017-2018 ', '0000-00-00 00:00:00'),
(152, 'BELINDA BREDZEI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KUUL FM', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(153, 'EBEN DEGON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KUUL FM', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(154, 'KOFI AWOONOR ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'LORLORNYO FM', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(155, 'BRIGHT ELLIS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'GLOBAL FM', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(156, 'FRANK FOLEY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/frank_foley.png', '', 'HERITAGE FM', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(157, 'ISRAEL ABOTSIVIA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KEKELI RADIO', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(158, 'KWABENA NTOW ', 0, 'http://mysmsinbox.com/rtp_api/nominees/kwabena_ntow.png', '', 'BEYOND FM', '', 'Volta Region', 'BEST RADIO PERSONALITY VOLTA REGION 2017-2018', '0000-00-00 00:00:00'),
(159, 'KOJO BRUCE', 0, 'http://mysmsinbox.com/rtp_api/nominees/kojo_bruce.png', '', 'SKYY POWER FM', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(160, 'KWAME BOAKYI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KYZZ FM', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(161, 'PAA KWESI SIMPSON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/paa_kwesi_simpson.png', '', 'CONNECT FM', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(162, 'YOOFI EYESON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/yoofi.png', '', 'SPICE FM', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(163, 'ROSLYN GYANE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rosaline_djan.png', '', 'EMPIRE FM', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(164, 'MAAME ESI SENUA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/maame_esi.png', '', 'RADIO MAX', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(165, 'KWAME BOAKYI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KYZZ FM', '', 'Western Region', 'BEST RADIO PERSONALITY WESTERN REGION 2017-2018', '0000-00-00 00:00:00'),
(166, 'DJ QUEST ', 0, 'http://mysmsinbox.com/rtp_api/nominees/quest.png', '', 'ARK FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(167, 'IKE FRIMPONG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/ike_frimpong.png', '', 'SKY FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(168, 'ALHAJI ALHASSAN YAHAYA', 0, 'http://mysmsinbox.com/rtp_api/nominees/alhaji_alhassan_yahaya.png', '', 'ARK FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(169, 'NANA YAW NKETIAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_yaw_nketiah.png', '', 'GIFT FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(170, 'COLINS APRAKU (ALUTA) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/colins_apraku_(aluta).png', '', 'MOONLIGHT FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(171, 'AKOSUA AFRIYIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/akosua_afriyie.png', '', 'ARK FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(172, 'AUDREY AKOSUA TINDANA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/audrey_akosua_tindana.png', '', 'ANGEL FM', '', 'Brong Ahafo Region', 'BEST RADIO PERSONALITY BRONG AHAFO REGION 2017-2018', '0000-00-00 00:00:00'),
(173, 'FRANCIS ABBAN ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(174, 'BERNARD AVLE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bernard_avle.png', '', 'CITI FM', '', 'Greater Accra', 'RADIO MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(175, 'ANDY DOSTY ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'HITZ FM', '', 'Greater Accra', 'RADIO MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(176, 'DANIEL DADZIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'RADIO MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(177, 'EKOUBA GYASI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ATINKA FM ', '', 'Greater Accra', 'RADIO MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(178, 'KWASI ABOAGYIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'NEAT FM', '', 'Greater Accra', 'RADIO MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(179, 'KOKROKOO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'RADIO MORNING SHOW OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(180, 'DWASO NSEM ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ADOM FM ', '', 'Greater Accra', 'RADIO MORNING SHOW OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(181, 'MORNING STARR ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STAR FM', '', 'Greater Accra', 'RADIO MORNING SHOW OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(182, 'CITI BREAKFAST SHOW ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'CITI FM', '', 'Greater Accra', 'RADIO MORNING SHOW OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(183, 'SUPER MORNING SHOW ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'RADIO MORNING SHOW OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(184, 'DAYBREAK HITZ ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'HITZ FM', '', 'Greater Accra', 'RADIO MORNING SHOW OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(185, 'NANA DARKWA GYASI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'RADIO GOLD ', '', 'Greater Accra', 'SPORTS SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(186, 'DAN KWAKU YEBOAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'SPORTS SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(187, 'BENEDICT OWUSU ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'SPORTS SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(188, 'MICHAEL DARKO (SUMMER) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/michael_darko.png', '', 'ANGEL FM ', '', 'ASHANTI REGION', 'SPORTS SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(189, 'BRIGHT KANKAM BOADU ', 0, 'http://mysmsinbox.com/rtp_api/nominees/bright_kankam_boadu.png', '', 'NHYIRA FM ', '', 'ASHANTI REGION', 'SPORTS SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(190, 'GEORGE ADDO JUNIOR ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM/HITZ FM', '', 'Greater Accra', 'SPORTS SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(191, 'STARR SPORTS ', 1, 'http://mysmsinbox.com/rtp_api/npminees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO SPORTS PROGRAM OF THE YEAR 2017-2018 ', '0000-00-00 00:00:00'),
(192, 'ANGEL SPORTS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/angel_fm.png', '', 'ANGEL FM ', '', 'Ashanti Region', 'RADIO SPORTS PROGRAM OF THE YEAR 2017-2018 ', '0000-00-00 00:00:00'),
(193, 'PEACE POWER SPORTS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/peace_power_sports', '', 'PEACE FM', '', 'Greater Accra', 'RADIO SPORTS PROGRAM OF THE YEAR 2017-2018 ', '0000-00-00 00:00:00'),
(194, 'ASEMPA SPORTS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ASEMPA FM', '', 'Greater Accra', 'RADIO SPORTS PROGRAM OF THE YEAR 2017-2018 ', '0000-00-00 00:00:00'),
(195, 'NHYIRA SPORTS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'NHYIRA FM ', '', 'Ashanti Region', 'RADIO SPORTS PROGRAM OF THE YEAR 2017-2018 ', '0000-00-00 00:00:00'),
(196, 'POWER SPORTS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'POWER FM', '', 'Greater Accra', 'RADIO SPORTS PROGRAM OF THE YEAR 2017-2018 ', '0000-00-00 00:00:00'),
(197, 'BONOHENE BAFFOUR AWUAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'KASAPA FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (LOCAL LANGUAGE)', '0000-00-00 00:00:00'),
(198, 'AFIA POKUAA', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (LOCAL LANGUAGE)', '0000-00-00 00:00:00'),
(199, 'OHENE NANA KWAME AMOH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM ', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (LOCAL LANGUAGE)', '0000-00-00 00:00:00'),
(200, 'KWADWO DICKSON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (LOCAL LANGUAGE)', '0000-00-00 00:00:00'),
(201, 'AKOSUA AGO ABOAGYIE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (LOCAL LANGUAGE)', '0000-00-00 00:00:00'),
(202, 'KOFI ADOMA NWANWANI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (LOCAL LANGUAGE)', '0000-00-00 00:00:00'),
(203, 'NAA DEDEI TETTEH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (ENGLISH LANGUAGE)', '0000-00-00 00:00:00'),
(204, 'BERNICE ABU BAIDDOO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (ENGLISH LANGUAGE)', '0000-00-00 00:00:00'),
(205, 'EMEFA APAWU ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (ENGLISH LANGUAGE)', '0000-00-00 00:00:00'),
(206, 'ATIEWIN MBILLAH-LAWSON ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (ENGLISH LANGUAGE)', '0000-00-00 00:00:00'),
(207, 'KWAKU OBENG AGYEI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (ENGLISH LANGUAGE)', '0000-00-00 00:00:00'),
(208, 'EVANS MENSAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM', '', 'Greater Accra', 'RADIO NEWSCASTER OF THE YEAR (ENGLISH LANGUAGE)', '0000-00-00 00:00:00'),
(209, 'MIDDAY NEWS ON PEACE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'RADIO NEWS PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(210, 'EYE WITNESS NEWS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'CITI FM', '', 'Greater Accra', 'RADIO NEWS PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(211, 'STARR NEWS TODAY ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO NEWS PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(212, 'TOP STORY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM ', '', 'Greater Accra', 'RADIO NEWS PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(213, 'KASIEBO IS TASTY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO NEWS PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(214, 'NEWS WIRE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'RADIO XYZ', '', 'Greater Accra', 'RADIO NEWS PROGRAM OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(215, 'AGYEMANG PREMPEH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'POWER FM', '', 'Greater Accra', 'RADIO LATE AFTERNOON SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(216, 'GIOVANNI CALEB ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM ', '', 'Greater Accra', 'RADIO LATE AFTERNOON SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(217, 'BLAKK RASTA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ZYLOFON FM ', '', 'Greater Accra', 'RADIO LATE AFTERNOON SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(218, 'ANTOINE MENSAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'LIVE FM', '', 'Greater Accra', 'RADIO LATE AFTERNOON SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(219, 'ABEIKU SANTANA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'OKAY FM', '', 'Greater Accra', 'RADIO LATE AFTERNOON SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(220, 'MICKY DARLING ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'RADIO LATE AFTERNOON SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(221, 'KWASI ABOAGYE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM ', '', 'Greater Accra', 'RADIO ENTERTAINMENT TALK SHOW HOST', '0000-00-00 00:00:00'),
(222, 'AUSTIN WOOD ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ABUSUA FM', '', 'Ashanti Region', 'RADIO ENTERTAINMENT TALK SHOW HOST', '0000-00-00 00:00:00'),
(223, 'AGYEMANG PREMPEH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'POWER FM', '', 'Greater Accra', 'RADIO ENTERTAINMENT TALK SHOW HOST', '0000-00-00 00:00:00'),
(224, 'KWAME ADJETIA (AK) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'NEAT FM', '', 'Greater Accra', 'RADIO ENTERTAINMENT TALK SHOW HOST', '0000-00-00 00:00:00'),
(225, 'CHRISTIAN AGYEI FRIMPONG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ONUA FM ', '', 'Greater Accra', 'RADIO ENTERTAINMENT TALK SHOW HOST', '0000-00-00 00:00:00'),
(226, 'SAMMY FLEX ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'SAMMY FLEX ', '', 'Greater Accra', 'RADIO ENTERTAINMENT TALK SHOW HOST', '0000-00-00 00:00:00'),
(227, 'DJ CHAKKA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'POWER FM', '', 'Greater Accra', 'RADIO DJ OF THE YEAR ', '0000-00-00 00:00:00'),
(228, 'DJ VYRUSKI ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO DJ OF THE YEAR ', '0000-00-00 00:00:00'),
(229, 'DJ MIC SMITH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'YFM ', '', 'Greater Accra', 'RADIO DJ OF THE YEAR ', '0000-00-00 00:00:00'),
(230, 'DJ SLIM ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'YFM', '', 'ASHANTI REGION', 'RADIO DJ OF THE YEAR ', '0000-00-00 00:00:00'),
(231, 'MR. KAXTRO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ULTIMATE FM', '', 'Greater Accra', 'RADIO DJ OF THE YEAR ', '0000-00-00 00:00:00'),
(232, 'VISION DJ ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'YFM', '', 'Greater Accra', 'RADIO DJ OF THE YEAR ', '0000-00-00 00:00:00'),
(233, 'MIRIAM OSEI AGYEMANG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/miriam_agyemang.png', '', '3FM', '', 'Greater Accra', 'RADIO FEMALE PRESENTER OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(234, 'AKUMAA MAMA ZIMBI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/akumaa_mama_zimbi.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO FEMALE PRESENTER OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(235, 'AFIA POKUAA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO FEMALE PRESENTER OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(236, 'JOSELYN DUMAS ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'JOY FM ', '', 'Greater Accra', 'RADIO FEMALE PRESENTER OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(237, 'OHEMAA KENTE', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ANGEL FM ', '', 'Ashanti Region', 'RADIO FEMALE PRESENTER OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(238, 'AKOSUA AGOR ABOAGYE ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM/OKAY FM', '', 'Greater Accra', 'RADIO FEMALE PRESENTER OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(239, 'JOHNNY HUGHES ', 0, 'http://mysmsinbox.com/rtp_api/nominees/johnnie_hughes_tv3.png', '', '3FM', '', 'Greater Accra', 'RADIO DEVT SHOW HOST OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(240, 'NYANSA BOAKWAH ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'HAPPY FM', '', 'Greater Accra', 'RADIO DEVT SHOW HOST OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(241, 'NANA YAA KONADU', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM', '', 'Greater Accra', 'RADIO DEVT SHOW HOST OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(242, 'AKUMAA MAMA ZIMBI', 1, 'http://mysmsinbox.com/rtp_api/nominees/akumaa_mama_zimbi.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO DEVT SHOW HOST OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(243, 'BRIGHT ASEMPA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ONUA FM', '', 'Greater Accra', 'RADIO DEVT SHOW HOST OF THE YEAR 2017-2018', '0000-00-00 00:00:00'),
(244, 'NANA YAW SARFO ', 0, 'http://mysmsinbox.com/rtp_api/nominees/nana_sarfo.png', '', 'VISION 1 FM', '', 'Greater Accra', 'RADIO MID-MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(245, 'KOFI OTCHERE DARKO (KOD) ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'STARR FM', '', 'Greater Accra', 'RADIO MID-MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(246, 'MIRIAM OSEI AGYEMANG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/miriam_agyemang.png', '', '3FM', '', 'Greater Accra', 'RADIO MID-MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(247, 'KOFI KUM BILSON ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PEACE FM ', '', 'Greater Accra', 'RADIO MID-MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(248, 'ALEX AYENSU BAAH (OKYEAME) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'RADIO GOLD', '', 'Greater Accra', 'RADIO MID-MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(249, 'KOJO ADJEI ABOAKYI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'BEACH FM ', '', 'WESTERN REGION', 'RADIO MID-MORNING SHOW HOST OF THE YEAR', '0000-00-00 00:00:00'),
(250, 'AFRICAN CHILD ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'LUV FM/NHYIRA FM', '', 'Greater Accra', 'RADIO REGGAE SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(251, 'CONSCIOUS QUEEN ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'PINK FM', '', 'Greater Accra', 'RADIO REGGAE SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(252, 'DORIS LOMO ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'RADIO GOLD', '', 'Greater Accra', 'RADIO REGGAE SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(253, 'KING LAGAZI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'HITZ FM', '', 'Greater Accra', 'RADIO REGGAE SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(254, 'BLAKK RASTA ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ZYLOFON FM', '', 'Greater Accra', 'RADIO REGGAE SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(255, 'OB NARTEY ', 0, 'http://mysmsinbox.com/rtp_api/nominees/ob_nartey.png', '', 'VISION 1 FM', '', 'Greater Accra', 'RADIO GOSPEL SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(256, 'FRANK KWABENA OWUSU (FRANKY5) ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'HITZ FM', '', 'Greater Accra', 'RADIO GOSPEL SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(257, 'KOJO OPPONG ADJEI ', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'SUNNY FM', '', 'Greater Accra', 'RADIO GOSPEL SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(258, 'JESHURUN OKYERE ', 1, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'LIVE FM', '', 'Greater Accra', 'RADIO GOSPEL SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(259, 'KWAMINA IDAN', 0, 'http://mysmsinbox.com/rtp_api/nominees/rtp_logo.png', '', 'ADOM FM', '', 'Greater Accra', 'RADIO GOSPEL SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00'),
(260, 'AGYENIM BOATENG ', 0, 'http://mysmsinbox.com/rtp_api/nominees/agyen.png', '', 'KASAPA FM', '', 'Greater Accra', 'RADIO GOSPEL SHOW HOST OF THE YEAR ', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rtp_pay`
--

CREATE TABLE `rtp_pay` (
  `pay_id` int(11) NOT NULL,
  `response_code` text,
  `amt_after_charges` text,
  `transaction_id` text,
  `client_reference` text,
  `description` text,
  `external_trans_id` text,
  `amount` float DEFAULT NULL,
  `number_of_votes` int(11) NOT NULL,
  `charges` float DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `when` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rtp_pay`
--

INSERT INTO `rtp_pay` (`pay_id`, `response_code`, `amt_after_charges`, `transaction_id`, `client_reference`, `description`, `external_trans_id`, `amount`, `number_of_votes`, `charges`, `number`, `when`) VALUES
(1, '2001', '0', '7cf07acf48f74108b4448c56d2d00e00', '23213', 'Transaction failed due to an error with the Payment Processor. Please review your request or retry in a few minutes.', '4011932938', 15, 50, 0, '0244632692', ''),
(2, '0000', '14.7075', 'b3cfc6b9aed744978a8a94ef2f62df1f', '23213', 'The MTN Mobile Money payment has been approved and processed successfully.', '4012635970', 15, 450, 0.2925, '0244632692', ''),
(3, '0000', '0', '44611b859caf45c8982192d268b7b079', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 650, 0, '123456789', ''),
(4, '0000', '0', '2924380722f84da9888aef7f65e942c3', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(5, '2051', '0', 'b5f27e4f22e746549a3c3283910ac56c', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(6, '2051', '0', 'e5e4eb79e0c644298f900a3eacd8a923', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(7, '2051', '0', '840c7036c61b4520a16e9c7a2119916f', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(8, '2051', '0', '4745466e0d7848709940bf238afe2047', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(9, '2051', '0', '0b9b1a0b5cda486a87817ad808317541', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(10, '2051', '0', '89c43aa2c7e8424fa125f78e8787e4a7', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(11, '2051', '0', '8989717ae14b4aee9f73c77ef0510290', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(12, '2051', '0', 'bb19b27301fe4160b2b3f9d975733629', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(13, '2051', '0', '549681a2df844fa1b7ce907742f01738', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(14, '2051', '0', '5ab86e2e616c497cb922a88711011230', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(15, '2051', '0', 'da623f4965f34e0b96761915914357ca', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(16, '2051', '0', 'c35867517b64447e89a63be12b72e8af', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(17, '2051', '0', 'ec0d4f3e23d8402c91c0af3c24dd2566', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(18, '2051', '0', '020d17ed690b44e899afbc26d75427ed', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(19, '2051', '0', '9212b589bc654dfe8de2eeef88ee5f76', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(20, '2051', '0', 'cc19bd7f4a754d9d968da3c1f683a4f7', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(21, '2051', '0', '8b7df34bd6954197ba38e1a2e3784287', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(22, '2051', '0', '4404ddcba31b4128b5138fd39524e8b1', '23213', 'The mobile number provided is not registered on MTN Mobile Money.', '', 15, 50, 0, '0540106459', ''),
(23, '2001', '0', 'ad5854ba4c564971867c3d57bf0cd0b0', '23213', 'Transaction status check returned not found', '', 15, 50, 0, '0540106459', ''),
(24, '2150', '0', 'ad5854ba4c564971867c3d57bf0cd0b0', '23213', 'The request has expired as the Tigo Cash subscriber did not approve payment within 10 minutes.', '1651A5B3A7863185', 15, 50, 0, '0540106459', '');

-- --------------------------------------------------------

--
-- Table structure for table `sms_pay`
--

CREATE TABLE `sms_pay` (
  `sms_pay_id` int(11) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `when` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_pay`
--

INSERT INTO `sms_pay` (`sms_pay_id`, `sender`, `message`, `when`) VALUES
(1, '233540106459', 'TV STATION OF THE YEAR,ADOM TV', '2018-08-03 13:00:43'),
(2, '233540106459', 'Digital TV Channel Of The Year,AMANSAN TV', '2018-08-03 13:01:10'),
(3, '233540109010', 'TV STATION OF THE YEAR,ADOM TV', '2018-08-07 09:11:15'),
(4, '233540116640', 'TV Sports Program Of The Year 2017-2018,HEAD START', '2018-08-07 09:53:15'),
(5, '233540116640', 'Media Group Of The Year,MULTIMEDIA GROUP', '2018-08-07 10:09:32'),
(6, '233540116640', 'Media Group Of The Year,MULTIMEDIA GROUP', '2018-08-07 10:10:28'),
(7, '233544336599', 'rtp', '2018-08-07 10:35:44'),
(8, '233544336599', 'TV STATION OF THE YEAR,ADOM TV', '2018-08-07 10:38:42'),
(9, '233544336599', 'Media Group Of The Year,MULTIMEDIA GROUP', '2018-08-07 10:46:43'),
(10, '233544336599', 'TV STATION OF THE YEAR,ADOM TV', '2018-08-07 10:52:49'),
(11, '233540106459', 'Media Group Of The Year,MULTIMEDIA GROUP', '2018-08-07 11:22:37'),
(12, '233540106459', 'TV STATION OF THE YEAR,ADOM TV', '2018-08-07 13:09:44'),
(13, '233544336599', 'Media Group Of The Year,MULTIMEDIA GROUP', '2018-08-07 13:15:41'),
(14, '233540106459', 'RTP A1  UTV', '2018-08-07 17:16:33'),
(15, '233540106459', 'Rtp a1 utv', '2018-08-07 17:25:49'),
(16, '233540106459', 'Rtp a1 utv', '2018-08-07 17:30:01'),
(17, '233540106459', 'Rtp a1 utv', '2018-08-07 17:53:22'),
(18, '233540106459', 'Rtp a1 utv', '2018-08-07 18:09:42'),
(19, '233540106459', 'Rtp a1 utv', '2018-08-07 18:10:52'),
(20, '233540106459', 'Rtp a1 utv', '2018-08-07 18:16:43'),
(21, '233540106459', 'RTP A1  UTV', '2018-08-07 18:38:13'),
(22, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 04:42:26'),
(23, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 04:46:44'),
(24, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 04:47:37'),
(25, '233540106459', 'RTP A1 uTV', '2018-08-08 04:48:03'),
(26, '233540106459', 'RTP A1  uTV', '2018-08-08 04:48:40'),
(27, '233540106459', 'RTP A1  uTV', '2018-08-08 04:49:38'),
(28, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 04:50:44'),
(29, '233540106459', 'RTP A1 utv', '2018-08-08 04:51:08'),
(30, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 04:54:09'),
(31, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 04:54:57'),
(32, '233540106459', 'RTP A1  UTV', '2018-08-08 04:57:35'),
(33, '233540106459', 'RTP A3 TV XYZ', '2018-08-08 04:58:04'),
(34, '233540106459', 'RTP E1 BADWAM', '2018-08-08 05:06:38'),
(35, '233540106459', 'RTP E1 GH TODAY', '2018-08-08 05:07:04'),
(36, '233244480965', 'Happy birthday Grace Appiah Kuwornu of Lakeside Estate. From Ama Tutu and the Longrich team', '2018-08-08 05:07:42'),
(37, '233244480965', 'Happy birthday Pascaline Nana Adwoa Affoh of Lighthouse Chapel Community 8. From Kwame Alipui', '2018-08-08 05:11:06'),
(38, '233540106459', 'RTP A7 NANA YAW SARFO', '2018-08-08 07:21:41'),
(39, '233540102592', 'Rtp a1 utv', '2018-08-08 07:53:15'),
(40, '233540109010', 'RTP A1 ADOM TV', '2018-08-08 08:46:37'),
(41, '233540102592', 'Rtp a1 utv', '2018-08-08 09:02:30'),
(42, '233546829992', 'RTP a1 utv', '2018-08-08 09:02:34'),
(43, '233540102547', 'Rtp a1 utv', '2018-08-08 09:04:00'),
(44, '233540106459', 'Rtp A1 utv', '2018-08-08 09:04:50'),
(45, '233540106459', 'Rtp a1 kwese tv', '2018-08-08 09:06:14'),
(46, '233540109010', 'RTP A1  UTV', '2018-08-08 09:06:20'),
(47, '233540109010', 'RTP A1 UTV', '2018-08-08 09:07:15'),
(48, '233540106459', 'RTP A1  TV3', '2018-08-08 09:07:42'),
(49, '233546829992', 'RTP a1 utv', '2018-08-08 09:10:25'),
(50, '233540102592', 'Rtp a1 utv', '2018-08-08 09:13:03'),
(51, '233540102592', 'RTP a1 utv', '2018-08-08 09:14:28'),
(52, '233546829992', 'RTP a1 tv3', '2018-08-08 09:14:33'),
(53, '233540106459', 'RTP A1  TV3', '2018-08-08 10:11:45'),
(54, '233540106459', 'Rtp a1 utv', '2018-08-08 10:12:10'),
(55, '233540106459', 'rtp a1 utv', '2018-08-08 10:12:35'),
(56, '233540102592', 'Rtp a1 utv', '2018-08-08 10:13:15'),
(57, '233546829992', 'RTP a1 tv3', '2018-08-08 10:25:22'),
(58, '233546829992', 'Rtp a1 tv3', '2018-08-08 10:26:30'),
(59, '233546829992', 'Rtp a1 tvafrica', '2018-08-08 10:27:24'),
(60, '233540109010', 'RTP A5 ANDY DOSTY', '2018-08-08 13:12:05'),
(61, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 13:48:37'),
(62, '233544336599', 'rtp a1 utv', '2018-08-08 14:03:49'),
(63, '233544336599', 'Rtp a1 utv', '2018-08-08 14:14:47'),
(64, '233556487370', 'Rtp', '2018-08-08 14:20:48'),
(65, '233556487370', 'Rtp a1 utv', '2018-08-08 14:21:57'),
(66, '233544336599', 'RTP A1 UTV', '2018-08-08 14:24:26'),
(67, '233544336599', 'Rtp a1 utv', '2018-08-08 14:25:46'),
(68, '233540106459', 'Rtp A1 kewese tv', '2018-08-08 15:33:42'),
(69, '233540106459', 'RTP A1 KWESE TV', '2018-08-08 15:34:20'),
(70, '233544336599', 'Rtp a1 kwese tv', '2018-08-08 15:35:42'),
(71, '233544336599', 'rtp a1 utv', '2018-08-08 15:36:19'),
(72, '233544336599', 'rtp a1 utv', '2018-08-08 16:44:12'),
(73, '233246001756', 'Please where is Paster Ranford', '2018-08-09 05:54:30'),
(74, '233240299122', 'RTP A1 GHONE TV', '2018-08-09 16:47:43'),
(75, '233240299122', 'RTP A2 EIB NETWORK', '2018-08-09 16:48:12'),
(76, '233240299122', 'RTP A3 AGOO TV', '2018-08-09 16:48:41'),
(77, '233240299122', 'RTP A4 BERNARD AVLE', '2018-08-09 16:49:08'),
(78, '233240299122', 'RTP A5 FRANCIS ABBAN', '2018-08-09 16:49:29'),
(79, '233240299122', 'RTP A6 GIOVANNI CALEB', '2018-08-09 16:49:56'),
(80, '233240299122', 'RTP A7 KOFI OTCHERE DARKO (KOD)', '2018-08-09 16:50:14'),
(81, '233240299122', 'RTP A8 DJ VYRUSKI', '2018-08-09 16:50:42'),
(82, '233240299122', 'RTP B1 DORIS LOMO', '2018-08-09 16:51:38'),
(83, '233240299122', 'RTP B2 JESHURUN OKYERE', '2018-08-09 16:51:59'),
(84, '233240299122', 'RTP B4 BONOHENE BAFFOUR AWUAH', '2018-08-09 16:52:35'),
(85, '233240299122', 'RTP B5 ATIEWIN MBILLAH-LAWSON', '2018-08-09 16:53:02'),
(86, '233240299122', 'RTP B6 AKUMAA MAMA ZIMBI', '2018-08-09 16:53:32'),
(87, '233240299122', 'RTP B7 STARR CHART', '2018-08-09 16:53:56'),
(88, '233240299122', 'RTP B8 STARR SPORTS', '2018-08-09 16:54:07'),
(89, '233240299122', 'RTP B9 STARR NEWS TODAY', '2018-08-09 16:54:31'),
(90, '233240299122', 'RTP C6 FRANCIS ABBAN', '2018-08-09 16:54:56'),
(91, '233240299122', 'RTP C8 MORNING STARR', '2018-08-09 16:55:11'),
(92, '233244422446', 'Raymond, clarify if the million is you mention is in GHS or USD or Euro please. Thank you. Edem, Paraku Estates', '2018-08-10 08:25:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rtp_categories`
--
ALTER TABLE `rtp_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `rtp_dashboard_users`
--
ALTER TABLE `rtp_dashboard_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `rtp_nominees`
--
ALTER TABLE `rtp_nominees`
  ADD PRIMARY KEY (`nominee_id`);

--
-- Indexes for table `rtp_pay`
--
ALTER TABLE `rtp_pay`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `sms_pay`
--
ALTER TABLE `sms_pay`
  ADD PRIMARY KEY (`sms_pay_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rtp_categories`
--
ALTER TABLE `rtp_categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `rtp_dashboard_users`
--
ALTER TABLE `rtp_dashboard_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rtp_nominees`
--
ALTER TABLE `rtp_nominees`
  MODIFY `nominee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `rtp_pay`
--
ALTER TABLE `rtp_pay`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sms_pay`
--
ALTER TABLE `sms_pay`
  MODIFY `sms_pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
