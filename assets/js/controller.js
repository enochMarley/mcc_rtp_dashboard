getCategories();

function getMobileMoneyTextersLeaderBoard() {
    $.get('controllers/get-mobile-money-texters-leaderboard.php', function(response){
        var ctx = document.getElementById("rptpaygraph").getContext('2d');
        drawBarChart(ctx, response.data.data, response.data.labels);
    })
}

function getSMSTextersLeaderBoard() {
    $.get('controllers/get-sms-texters-leaderboard.php', function(response){
        var ctx = document.getElementById("smspaygraph").getContext('2d');
        drawBarChart(ctx, response.data.data, response.data.labels);
    })
}

function getDashboardSummary(){
    $.get('controllers/get-dashboard-summary.php', function(response) {
        if (response.success) {
            $('.number-of-categories').html(response.data.numberOfCategories);
            $('.number-of-nominees').html(response.data.getNUmberOfNominees);
            $('.number-of-votes').html(parseInt(response.data.numberOfSMSVotes) + parseInt(response.data.numberOfRTPVotes));
            $('.number-of-sms-votes').html(`${response.data.numberOfSMSVotes}`);
            $('.number-of-mom-votes').html(`${response.data.numberOfRTPVotes}`);
        }
    })
}

function getCategories() {
    $.get('controllers/get-categories.php', function(response){
        if (response.success) {
            var template = ``;
            if (response.data.length > 0) {
                var counter = 1;
                var anim_count = 0.1;
                template +=`
                    <table class='cat-table'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Category Title</th>
                                <th>Category Code</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    <tbody>
               `;

                response.data.forEach(category => {
                    template += `
                    <tr class="wow zoomIn" data-wow-duration='0.5s' data-wow-delay='${anim_count}s'>
                        <td>${counter}</td>
                        <td>${category.category_title}</td>
                        <td>${category.category_code}</td>
                        <td><a onclick=setCategoryCode('${category.category_code}') class='cat-detail-link'>View Statistics</a></td>
                    </tr>
                    `;

                    counter += 1 ;
                    anim_count += 0.05;
                });

                template += `
                        </tbody>
                    </table>
                `;
            } else {

            }

            $('.categories-res').html(template);
        } else {
            
        }
    })
}

function setCategoryCode(categoryTitle) {
    localStorage.removeItem('categoryCode');
    localStorage.setItem('categoryCode', categoryTitle);
    window.location.href = 'category-statistics.php';
}

function getCategoryStatistics() {
    var categoryCode = localStorage.getItem('categoryCode');
    var data = {
        categoryCode: categoryCode
    }

    $.post('controllers/get-category-statistics.php', data, function(response) {
        if (response.success) {
            var template = ``;
            if (response.data.length > 0) {
                var counter = 1;
                template +=`
                    <table class='cat-table'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nominee Image</th>
                                <th>Nominee Name</th>
                                <th>Nominee Votes</th>
                            </tr>
                        </thead>
                    <tbody>
               `;

                response.data.forEach(nominee => {
                    template += `
                    <tr>
                        <td>${counter}</td>
                        <td><img src="${nominee.nominee_thumbnail}" class="nominee-thumbnail"></td>
                        <td>${nominee.nominee_name}</td>
                        <td>${nominee.nominee_votes}</td>
                    </tr>
                    `;

                    counter += 1 ;
                });

                template += `
                        </tbody>
                    </table>
                `;
            } else {

            }

            $('.nominees-table-res').html(template);

            var ctx = document.getElementById("nomineeChart").getContext('2d');
            drawBarChart(ctx, response.graph.data, response.graph.labels);


            $('.cateogory-title').html(response.categoryTitle);
        } else {
            
        }
    })
}